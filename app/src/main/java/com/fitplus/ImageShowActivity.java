package com.fitplus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import model.FitDayModel;
import utils.Prefs;

public class ImageShowActivity extends AppCompatActivity {
    private Intent mainIntent;
    public static Activity mActivity;

    ImageView imge;
    private Bundle bundle;
    private FitDayModel fitday;
    TextView comment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.image);
        mActivity = null;

        bundle = getIntent().getExtras();
        fitday = bundle.getParcelable("FitDay");
        initView();
    }

    private void initView() {
        imge = (ImageView) findViewById(R.id.image);
        comment = (TextView) findViewById(R.id.comment);
        comment.setText(fitday.comment + "");
        if (fitday != null) {
            try {
                AppDelegate.getInstance(this).getImageLoader().get(fitday.file_name, new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AppDelegate.LogE(error);
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            imge.setImageBitmap(response.getBitmap());
                        }
                    }
                });
            } catch (Exception e) {
                AppDelegate.LogE(e);
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mActivity != null) {
            mActivity = null;
        }
    }
}
