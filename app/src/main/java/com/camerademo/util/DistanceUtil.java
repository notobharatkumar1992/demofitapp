package com.camerademo.util;


import com.fitplus.AppDelegate;

public class DistanceUtil {

    public static int getCameraAlbumWidth() {
        return (AppDelegate.getAppDelegate().getScreenWidth() - AppDelegate.getAppDelegate().dp2px(10)) / 4 - AppDelegate.getAppDelegate().dp2px(4);
    }
    
    // 相机照片列表高度计算 
    public static int getCameraPhotoAreaHeight() {
        return getCameraPhotoWidth() + AppDelegate.getAppDelegate().dp2px(4);
    }
    
    public static int getCameraPhotoWidth() {
        return AppDelegate.getAppDelegate().getScreenWidth() / 4 - AppDelegate.getAppDelegate().dp2px(2);
    }

    //活动标签页grid图片高度
    public static int getActivityHeight() {
        return (AppDelegate.getAppDelegate().getScreenWidth() - AppDelegate.getAppDelegate().dp2px(24)) / 3;
    }
}
