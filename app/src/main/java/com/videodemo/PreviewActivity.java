package com.videodemo;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.fitplus.AppDelegate;
import com.fitplus.R;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import utils.Prefs;

/**
 * Created by Bharat on 08/15/2016.
 */
public class PreviewActivity extends FragmentActivity implements View.OnClickListener, OnReciveServerResponse {

    private ImageView img_captured, img_dustbin, img_text, img_icon, img_download, img_attach, img_send;

    private int type = 0;
    private String filePath = "";
    private File file;

    private VideoView videoView;

    private boolean fileDelete = true;
    private boolean fileSaved = false;

    public static final int FILE_TYPE_IMAGE = 0, FILE_TYPE_VIDEO = 1;

    private LinearLayout ll_view;
    private RelativeLayout rl_view;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.image_preview_activity);
        type = getIntent().getExtras().getInt("from");
        filePath = getIntent().getExtras().getString("file");
        file = new File(filePath);
        initView();
        setValues();
    }

    private void setValues() {
        if (type == FILE_TYPE_IMAGE) {
            img_captured.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.GONE);
            img_captured.setImageBitmap(VideoRecordingPreLollipopActivity.decodeFile(file));
        } else {
            img_captured.setVisibility(View.GONE);
            videoView.setVisibility(View.VISIBLE);
            videoView.setMediaController(new MediaController(this));
            videoView.setVideoURI(Uri.fromFile(file));
            videoView.requestFocus();
            videoView.start();
        }
    }

    public static Bitmap mark(Bitmap src, String watermark, Point location, int color, int alpha, int size, boolean underline) {
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());

        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(src, 0, 0, null);

        Paint paint = new Paint();
        paint.setColor(color);
        paint.setAlpha(alpha);
        paint.setTextSize(size);
        paint.setAntiAlias(true);
        paint.setUnderlineText(underline);
        canvas.drawText(watermark, location.x, location.y, paint);

        return result;
    }

    private int _xDelta;
    private int _yDelta;

    private void initView() {
        img_captured = (ImageView) findViewById(R.id.img_captured);
        videoView = (VideoView) findViewById(R.id.videoView);

        img_dustbin = (ImageView) findViewById(R.id.img_dustbin);
        img_dustbin.setOnClickListener(this);
        img_text = (ImageView) findViewById(R.id.img_text);
        img_text.setOnClickListener(this);
        img_icon = (ImageView) findViewById(R.id.img_icon);
        img_icon.setOnClickListener(this);
        img_download = (ImageView) findViewById(R.id.img_download);
        img_download.setOnClickListener(this);
        img_attach = (ImageView) findViewById(R.id.img_attach);
        img_attach.setOnClickListener(this);
        img_send = (ImageView) findViewById(R.id.img_send);
        img_send.setOnClickListener(this);

        rl_view = (RelativeLayout) findViewById(R.id.rl_view);
        ll_view = (LinearLayout) findViewById(R.id.ll_view);
        ll_view.setVisibility(View.GONE);
        ll_view.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent event) {
                final int X = (int) event.getRawX();
                final int Y = (int) event.getRawY();
                switch (event.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_DOWN:
                        RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        _xDelta = X - lParams.leftMargin;
                        _yDelta = Y - lParams.topMargin;
                        break;
                    case MotionEvent.ACTION_UP:
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                        int left, top, right, bottom;

                        left = X - _xDelta;
                        top = Y - _yDelta;
                        right = -250;
                        bottom = -250;
                        layoutParams.leftMargin = 0;
                        layoutParams.topMargin = top;
                        layoutParams.rightMargin = 0;
                        layoutParams.bottomMargin = bottom;
                        Log.d("test", "margin => " + layoutParams.leftMargin + ", " + layoutParams.topMargin + ", " + layoutParams.rightMargin + ", " + layoutParams.bottomMargin);
                        view.setLayoutParams(layoutParams);
                        break;
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_dustbin:
                try {
                    if (file != null)
                        file.delete();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                finish();
                break;

            case R.id.img_text:
//                showTextAlert();
                ll_view.setVisibility(ll_view.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);

                break;

            case R.id.img_icon:
                break;

            case R.id.img_download:
                if (!fileSaved) {
                    fileSaved = true;
                    fileDelete = false;
                    Toast.makeText(PreviewActivity.this, type == FILE_TYPE_IMAGE ? "Image" : "Video" + " saved at location : " + file.getAbsolutePath(), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(PreviewActivity.this, type == FILE_TYPE_IMAGE ? "Image" : "Video" + " already saved.", Toast.LENGTH_LONG).show();
                }
                break;

            case R.id.img_attach:
                break;

            case R.id.img_send:
                // Send code
//                executeSend();

//                getBitmapFromView(ll_view);
//
//                getBitmapFromFile(filePath);
//                overlay(getBitmapFromFile(filePath), getBitmapFromView(ll_view));

                storeImage(filePath, overlay(getBitmapFromFile(filePath), getBitmapFromView(rl_view)));
                break;
        }
    }

    private void storeImage(String filePath, Bitmap image) {
        File pictureFile = new File(getExternalFilesDir(null), "pic" + "" + ".jpg");
        if (pictureFile == null) {
            Log.d("test", "Error creating media file, check storage permissions: ");// e.getMessage());
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 90, fos);
            fos.close();
            Log.d("test", "File saved successfully : " + filePath);
        } catch (FileNotFoundException e) {
            Log.d("test", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("test", "Error accessing file: " + e.getMessage());
        }
    }

    public static Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, 0, 0, null);
        return bmOverlay;
    }

    public static Bitmap getBitmapFromFile(String photoPath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(photoPath, options);
        return bitmap;
    }

    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }


    private void executeSend() {
        try {
            if (AppDelegate.haveNetworkConnection(this, true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(this).getUserdata().userId);
//                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.file, getRealPathFromURI(MainActivity.this, uri), ServerRequestConstants.Key_PostFileValue);
                AppDelegate.LogT("file path => " + new File(filePath));
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.uploadedfile0, filePath, ServerRequestConstants.Key_PostFileValue);
                AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.comment, shareText + "");
                mPostAsyncObj = new PostAsync(this, this, ServerRequestConstants.FIT_DAY_UPLOAD,
                        mPostArrayList, null, true);
                AppDelegate.showProgressDialog(this);
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(this);
        if (apiName.equalsIgnoreCase(ServerRequestConstants.FIT_DAY_UPLOAD)) {
            parseFitDay(result);
        }
    }

    private void parseFitDay(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getString(Tags.status).equalsIgnoreCase("0")) {
                AppDelegate.ShowDialog(this, jsonObject.getString(Tags.message), "");
//                AppDelegate.ShowDialog(this, jsonObject.getString(Tags.message), "");
            } else {
                AppDelegate.showToast(this, jsonObject.getString(Tags.message));
                finish();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(this, "Please try after some time.", "");
//          AppDelegate.ShowDialog(this, "Please try after some time.", "Alert!!!");
        }
    }

    private String shareText = "";

    private void showTextAlert() {
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.dialog_text_share, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);
        final EditText userInput = (EditText) promptsView.findViewById(R.id.et_text);
        userInput.setText(shareText);
        userInput.setSelection(shareText.length());
        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // get user input and set it to result
                                // edit text
                                shareText = userInput.getText().toString();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    protected void onDestroy() {
        try {
            if (fileDelete) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();

    }
}
