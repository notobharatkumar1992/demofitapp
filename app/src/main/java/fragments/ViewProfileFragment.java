package fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.EditProfileActivity;
import com.fitplus.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Async.LocationAddress;
import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.PagerAdapter;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.FitDayModel;
import model.PostAysnc_Model;
import model.UserDataModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class ViewProfileFragment extends Fragment implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse, OnDialogClickListener {

    private View rootview;
    ImageView display_pic, background_img, back;
    TextView name, address, description, tagline, followers, about_title, about_desc, get, follow, txt_edit_profile;
    ViewPager video_list;
    private int login_id;
    private LinearLayout get_follow;
    private ViewPager sponcer;
    private PagerAdapter bannerPagerAdapter;
    private List<Fragment> bannerFragment = new ArrayList();

    LinearLayout pager_indicator;
    private int isfollowed;
    private ImageView edit;

    public static Handler mHandler;
    private Prefs prefs;
    private ImageView img_loading;
    private ScrollView scroll;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.user_view_profile, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        Bundle bundle = getArguments();
        login_id = bundle.getInt(Parameters.login_id);
        initView();
        setHandler();
        mHandler.sendEmptyMessage(1);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                AppDelegate.LogT("dispatchMessage handler calld");
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {
                    setprofile(new Prefs(getActivity()).getUserdata());
                } else if (msg.what == 2) {
                    setAddressFromGEOcoder(msg.getData());
                } else if (msg.what == 3) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            scroll.fullScroll(View.FOCUS_UP);//if you move at the end of the scroll
                            scroll.pageScroll(View.FOCUS_UP);//if you move at the middle of the scroll
                            scroll.smoothScrollTo(0, 0);
                            scroll.fullScroll(ScrollView.FOCUS_UP);
                        }
                    }, 500);
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppDelegate.LogT("ViewProfile onDestroyView called");
        mHandler = null;
    }

    private void initView() {
        scroll = (ScrollView) rootview.findViewById(R.id.scroll);
        background_img = (ImageView) rootview.findViewById(R.id.user_img);
        display_pic = (ImageView) rootview.findViewById(R.id.display_pic);
        back = (ImageView) rootview.findViewById(R.id.back);
        name = (TextView) rootview.findViewById(R.id.name);
        address = (TextView) rootview.findViewById(R.id.address);
        description = (TextView) rootview.findViewById(R.id.description);
        tagline = (TextView) rootview.findViewById(R.id.Tagline);
        followers = (TextView) rootview.findViewById(R.id.followers);
        about_title = (TextView) rootview.findViewById(R.id.about_title);
        about_desc = (TextView) rootview.findViewById(R.id.about_desc);
        video_list = (ViewPager) rootview.findViewById(R.id.video_list);
        get = (TextView) rootview.findViewById(R.id.get);
        get_follow = (LinearLayout) rootview.findViewById(R.id.get_follow);
        follow = (TextView) rootview.findViewById(R.id.follow);
        txt_edit_profile = (TextView) rootview.findViewById(R.id.txt_edit_profile);
        txt_edit_profile.setOnClickListener(this);
        edit = (ImageView) rootview.findViewById(R.id.edit_profile);
        edit.setOnClickListener(this);
        img_loading = (ImageView) rootview.findViewById(R.id.img_loading);
        if (login_id == new Prefs(getActivity()).getUserdata().userId) {
            get_follow.setVisibility(View.GONE);
            txt_edit_profile.setVisibility(View.VISIBLE);
        } else {
            get_follow.setVisibility(View.VISIBLE);
            txt_edit_profile.setVisibility(View.GONE);
        }
        pager_indicator = (LinearLayout) rootview.findViewById(R.id.pager_indicator);
        follow.setOnClickListener(this);
        get.setOnClickListener(this);
        back.setOnClickListener(this);
        setUiPageViewController();
    }

    private ImageView[] dots;
    private int dotsCount, item_position = 0, selected_tab = 0, click_type = 0;

    private void setUiPageViewController() {
        pager_indicator.removeAllViews();
        dotsCount = bannerFragment.size();
        if (dotsCount > 0) {
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(getActivity());
                dots[i].setImageDrawable(getResources().getDrawable(R.drawable.yellow_radius_square));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(AppDelegate.getInstance(getActivity()).dp2px(15), AppDelegate.getInstance(getActivity()).dp2px(15));
                params.setMargins(AppDelegate.getInstance(getActivity()).dp2px(5), 0, AppDelegate.getInstance(getActivity()).dp2px(5), 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(getResources().getDrawable(R.drawable.white_radius_square));
        }
        video_list.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switchBannerPage(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        switchBannerPage(0);
    }

    private void switchBannerPage(int position) {
        if (dotsCount > 0) {
            for (int i = 0; i < dotsCount; i++) {
                dots[i].setImageResource(R.drawable.yellow_radius_square);
            }
            dots[position].setImageResource(R.drawable.white_radius_square);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        execute_getprofile();
    }

    private void execute_getprofile() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, login_id);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.login_id, new Prefs(getActivity()).getUserdata().userId);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), ViewProfileFragment.this, ServerRequestConstants.GET_PROFILE,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_edit_profile:
                AppDelegate.LogT("edit clicked");
                startActivity(new Intent(getActivity(), EditProfileActivity.class));
                break;
            case R.id.get:
                break;
            case R.id.follow:
                if (follow.isSelected()) {
                    follow.setSelected(false);
                    if (isfollowed == 0) {
                        execute_follow_status("follow");
                    } else {
                        execute_follow_status("unfollow");
                    }
                } else {
                    if (isfollowed == 0) {
                        execute_follow_status("follow");
                    } else {
                        execute_follow_status("unfollow");
                    }
                    follow.setSelected(true);
                }
                break;
            case R.id.back:
                getFragmentManager().popBackStack();
                break;
        }
    }

    private void execute_follow_status(String status) {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, login_id);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.follower_id, new Prefs(getActivity()).getUserdata().userId);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.status, status);

                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), ViewProfileFragment.this, ServerRequestConstants.FOLLOW,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }


    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.months:

                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.GET_PROFILE)) {
            parsegetprofile(result);
        } else if (apiName.equals(ServerRequestConstants.FOLLOW)) {
            parseFOlLOW(result);
        }
    }

    private void parseFOlLOW(String result) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(result);
            AppDelegate.ShowDialogToast(getActivity(), jsonObject.getString(Tags.message), "Alert");
            if (jsonObject.getInt(Tags.status) == 1 && jsonObject.getInt(Tags.dataFlow) == 1) {
                isfollowed = jsonObject.getInt("flag");
                if (isfollowed == 0) {
                    follow.setText(getActivity().getResources().getString(R.string.follow));
                } else {
                    if (isfollowed == 1) {
                        follow.setText(getActivity().getResources().getString(R.string.unfollow));
                    }
                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void parsegetprofile(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            if (jsonObject.getInt(Tags.status) == 1) {
                JSONObject obj = jsonObject.getJSONObject(Tags.response);
                JSONObject object = obj.getJSONObject("user");
                JSONArray fitday = obj.getJSONArray("fitday");
                UserDataModel userDataModel = new UserDataModel();
                userDataModel.first_name = object.getString(Tags.first_name);
                userDataModel.last_name = object.getString(Tags.last_name);
                userDataModel.email = object.getString(Tags.email);
                userDataModel.password = object.getString(Tags.password);
                userDataModel.created = object.getString(Tags.created);
                userDataModel.str_Gender = object.getString(Tags.gender);
                userDataModel.userId = object.getInt(Tags.user_id);
                userDataModel.dob = object.getString(Tags.birthdate);
                userDataModel.nickname = object.getString(Tags.nick_name);
                userDataModel.avtar = object.getString(Tags.avtar);
                userDataModel.views = object.getString(Tags.views);
                userDataModel.follower_count = object.getInt(Tags.follower_count);
                userDataModel.profile_visibility = object.getInt(Tags.profile_visibility);
                userDataModel.presentation_vedio = object.getString(Tags.presentation_vedio);
                userDataModel.bank_account = object.getString(Tags.bank_account);
                userDataModel.certificates = object.getString(Tags.certificates);
                userDataModel.country_id = object.getString(Tags.country_id);
                userDataModel.role_id = object.getInt(Tags.role_id);
                userDataModel.state_id = object.getString(Tags.state_id);
                userDataModel.city_id = object.getString(Tags.city_id);
                userDataModel.post_code = object.getString(Tags.post_code);
                userDataModel.sex_group = object.getString(Tags.sex_group);
                userDataModel.fat_status = object.getString(Tags.fat_status);
                userDataModel.type = object.getString(Tags.type);
                userDataModel.token = object.getString(Tags.token);
                userDataModel.is_login = object.getInt(Tags.is_login);
                userDataModel.is_social = object.getInt(Tags.is_social);
                userDataModel.is_verified = object.getInt(Tags.is_verified);
                userDataModel.latitude = object.getDouble(Tags.latitude);
                userDataModel.longitude = object.getDouble(Tags.longitude);
                userDataModel.about_me = object.getString(Tags.about_me);
                userDataModel.is_followed = object.getInt(Tags.is_followed);
                //AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, ViewProfileFragment.this);
                isfollowed = userDataModel.is_followed;
                if (isfollowed == 0) {
                    follow.setText(getActivity().getResources().getString(R.string.follow));
                } else {
                    if (isfollowed == 1) {
                        follow.setText(getActivity().getResources().getString(R.string.unfollow));
                    }
                }
                setprofile(userDataModel);
                ArrayList<FitDayModel> fitDayModelArrayList = new ArrayList<>();
                //FitDayModel fitDayModel=new FitDayModel();
                for (int i = 0; i < fitday.length(); i++) {
                    JSONObject object1 = fitday.getJSONObject(i);
                    FitDayModel fitDayModel = new FitDayModel();
                    fitDayModel.id = object1.getInt(Tags.user_id);
                    fitDayModel.file_name = object1.getString(Tags.file_name);
                    fitDayModel.file_type = object1.getInt(Tags.file_type);
                    fitDayModel.user_id = object1.getInt(Parameters.user_id);
                    fitDayModel.view = object1.getInt(Tags.view);
                    fitDayModel.comment = object1.getString(Tags.comment);
                    fitDayModel.created = object1.getString(Tags.created);
                    fitDayModel.file_thumb = object1.getString(Tags.file_thumb);
                    fitDayModelArrayList.add(fitDayModel);
                }
                setfitday(fitDayModelArrayList);
            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialog(getActivity(), jsonObject.getString(Tags.message), "alert");
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null /*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*/) {
                    AppDelegate.showAlert(getActivity(), jsonObject.getString(Tags.message));
                }
            }
        } catch (Exception e) {
            AppDelegate.showAlert(getActivity(), "Response is not proper. Please try again later.");
            AppDelegate.LogE(e);
        }
    }

    private void setfitday(ArrayList<FitDayModel> fitDayModelArrayList) {
        for (int i = 0; i < fitDayModelArrayList.size(); i++) {
            Fragment fragment = new VideoFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable(Tags.FitDayModel, fitDayModelArrayList.get(i));
            fragment.setArguments(bundle);
            bannerFragment.add(fragment);
        }
        AppDelegate.LogT("bannerFragment. size==>" + bannerFragment.size() + "");
        bannerPagerAdapter = new PagerAdapter(getChildFragmentManager(), bannerFragment);
        video_list.setAdapter(bannerPagerAdapter);
        setUiPageViewController();
        mHandler.sendEmptyMessage(3);
        /*scroll.fullScroll(View.FOCUS_UP);//if you move at the end of the scroll
        scroll.pageScroll(View.FOCUS_UP);//if you move at the middle of the scroll
        scroll.smoothScrollTo(0, 0);*/
    }

    private void setprofile(UserDataModel userDataModel) {
        AppDelegate.LogE("setprofile called");
        if (userDataModel.avtar.isEmpty()) {

        } else {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    img_loading.setVisibility(View.VISIBLE);
                    AnimationDrawable frameAnimation = (AnimationDrawable) img_loading.getDrawable();
                    frameAnimation.setCallback(img_loading);
                    frameAnimation.setVisible(true, true);
                    frameAnimation.start();
                    ((Animatable) img_loading.getDrawable()).start();
                }
            });
            Picasso.with(getActivity()).load(userDataModel.avtar)
                    .placeholder(R.drawable.img) // optional
                    .error(R.drawable.img).into(new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    display_pic.setImageBitmap(bitmap);
                    try {
                        background_img.setImageBitmap(AppDelegate.blurRenderScript(getActivity(), bitmap));
                    } catch (Exception e) {
                        AppDelegate.LogE(e);
                    }
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            img_loading.setVisibility(View.GONE);
                        }
                    });
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    AppDelegate.LogT("onBitmapFailed");
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    AppDelegate.LogT("onPrepareLoad");
                }
            });

        }
        name.setText((AppDelegate.isValidString(userDataModel.first_name) ? userDataModel.first_name : "") + " " + (AppDelegate.isValidString(userDataModel.last_name) ? userDataModel.last_name : ""));
        about_title.setText("About " + userDataModel.first_name + ":");
        about_desc.setText(userDataModel.about_me + "");
        followers.setText(String.valueOf(userDataModel.follower_count) + "  " + getActivity().getResources().getString(R.string.followers));
        setloc(Double.parseDouble(new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, "")), Double.parseDouble(new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, "")));
        tagline.setText((AppDelegate.isValidString(userDataModel.fat_status) ? userDataModel.fat_status : "") + " ," + (AppDelegate.isValidString(userDataModel.sex_group) ? userDataModel.sex_group : "" )+ " ," + (AppDelegate.isValidString(userDataModel.type) ? userDataModel.type : " "));

    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, getActivity(), mHandler);
    }

    private void setAddressFromGEOcoder(Bundle data) {
        address.setText(data.getString(Tags.country_param) + "");
    }

    @Override
    public void setOnDialogClickListener(String name) {

    }
}

