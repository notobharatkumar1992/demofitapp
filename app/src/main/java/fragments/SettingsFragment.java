package fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.fitplus.AppDelegate;
import com.fitplus.BeginnerQuestionActivity;
import com.fitplus.LoginActivity;
import com.fitplus.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import Constants.Tags;
import adapters.MonthsAdapter;
import adapters.UploadDocsAdapter;
import model.UploadDocsModel;
import model.UserDataModel;
import utils.Prefs;

import static android.media.ExifInterface.ORIENTATION_NORMAL;
import static android.media.ExifInterface.TAG_ORIENTATION;

/**
 * Created by admin on 26-07-2016.
 */
public class SettingsFragment extends Fragment implements OnClickListener {
    private View rootview;
    private ImageView remember_me, notification;
    public TextView user_category, language;
    public Prefs prefs;
    private UserDataModel userDataModel;
    public static Handler mHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.setting, container, false);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        prefs = new Prefs(getActivity());
        userDataModel = prefs.getUserdata();
        initView();
        setHandler();
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                AppDelegate.LogT("dispatchMessage handler calld");
                if (msg.what == 10) {
                    AppDelegate.showProgressDialog(getActivity());
                } else if (msg.what == 11) {
                    AppDelegate.hideProgressDialog(getActivity());
                } else if (msg.what == 1) {
                    AppDelegate.LogT("set User category" + userDataModel.fat_status + (userDataModel.sex_group) + (userDataModel.type) + "");
                    userDataModel = prefs.getUserdata();
                    user_category.setText((AppDelegate.isValidString(userDataModel.fat_status) ? userDataModel.fat_status : "") + " ," + (AppDelegate.isValidString(userDataModel.sex_group) ? userDataModel.sex_group : "") + " ," + (AppDelegate.isValidString(userDataModel.type) ? userDataModel.type : " "));
                }
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppDelegate.LogT("ViewProfile onDestroyView called");
        mHandler = null;
    }

    private void initView() {
        rootview.findViewById(R.id.back).setOnClickListener(this);
        user_category = (TextView) rootview.findViewById(R.id.user_category);
        user_category.setOnClickListener(this);
        language = (TextView) rootview.findViewById(R.id.language);
        language.setOnClickListener(this);
        remember_me = (ImageView) rootview.findViewById(R.id.remember_me);
        remember_me.setOnClickListener(this);
        notification = (ImageView) rootview.findViewById(R.id.notification);
        notification.setOnClickListener(this);
        user_category.setText((AppDelegate.isValidString(userDataModel.fat_status) ? userDataModel.fat_status : "") + " ," + (AppDelegate.isValidString(userDataModel.sex_group) ? userDataModel.sex_group : "") + " ," + (AppDelegate.isValidString(userDataModel.type) ? userDataModel.type : " "));
        if (prefs.getnotification(Tags.notification_value)) {
            AppDelegate.LogT("notification on -====" + (prefs.getnotification(Tags.notification_value)));
            notification.setSelected(true);
        } else {
            AppDelegate.LogT("notification off-====" + (prefs.getnotification(Tags.notification_value)));
            notification.setSelected(false);
        }
        String emailid = AppDelegate.getValue(getActivity(), Tags.Email);
        if (AppDelegate.isValidString(emailid)) {
            AppDelegate.Log("emailid", emailid + "" + "hellooo");
            remember_me.setSelected(true);
        } else {
            AppDelegate.Log("emailid", emailid + "" + "byee");
            remember_me.setSelected(false);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                getFragmentManager().popBackStack();
                break;
            case R.id.user_category:
                AppDelegate.setting_or_not = true;
                startActivity(new Intent(getActivity(), BeginnerQuestionActivity.class));
                break;
            case R.id.remember_me:
                if (remember_me.isSelected()) {
                    remember_me.setSelected(false);
                    AppDelegate.remember_me = false;
                    AppDelegate.save(getActivity(), "", Tags.Email);
                    AppDelegate.save(getActivity(), "", Tags.Password);
                    AppDelegate.LogT("userdata is on=====" + AppDelegate.getValue(getActivity(), Tags.Email));
                    update();
                } else {
                    remember_me.setSelected(true);
                    AppDelegate.remember_me = true;
                    AppDelegate.LogT("userdata is off=====" + AppDelegate.getValue(getActivity(), Tags.Email));
                    update();
                }
                break;
            case R.id.notification:
                if (notification.isSelected()) {
                    prefs.putnotification(Tags.notification_value, false);
                    notification.setSelected(false);
                    AppDelegate.LogT("off=====" + prefs.getnotification(Tags.notification_value));

                } else {
                    prefs.putnotification(Tags.notification_value, true);
                    notification.setSelected(true);
                    AppDelegate.LogT("on=====" + prefs.getnotification(Tags.notification_value));
                }
                break;
        }
    }

    private void update() {
        if (LoginActivity.mHandler != null) {
            AppDelegate.LogT("updateGlobal handler calld");
            LoginActivity.mHandler.sendEmptyMessage(1);
        }
    }

}
