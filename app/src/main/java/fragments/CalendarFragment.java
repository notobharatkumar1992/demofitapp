package fragments;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.Recycler_Calendar_dates_Adapter;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.ScheduledListModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class CalendarFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse {
    private Calendar currentCalender = Calendar.getInstance(Locale.getDefault());
    private RecyclerView recycler_dates;
    private CompactCalendarView date;
    private TextView month;
    private SimpleDateFormat month_date;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.calendar_frag, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        executeScheduleddates();
//
    }

  /*  private void testAddEvent() {

        addEvents(calendar.getTime());
        date.invalidate();

        logEventsByMonth(date);
    }*/

    private SimpleDateFormat dateFormatForDisplaying = new SimpleDateFormat("dd-M-yyyy hh:mm:ss a", Locale.getDefault());

    private void logEventsByMonth(CompactCalendarView compactCalendarView) {
        currentCalender.setTime(new Date());
        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
        currentCalender.set(Calendar.MONTH, Calendar.AUGUST);
        List<String> dates = new ArrayList<>();
        for (Event e : compactCalendarView.getEventsForMonth(new Date())) {
            dates.add(dateFormatForDisplaying.format(e.getTimeInMillis()));
        }
        Log.d("test", "Events for Aug with simple date formatter: " + dates);
        Log.d("test", "Events for Aug month using default local and timezone: " + compactCalendarView.getEventsForMonth(currentCalender.getTime()));
    }


    private void executeScheduleddates() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), CalendarFragment.this, ServerRequestConstants.SCHEDULED_DATES,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }

    }

    private void initView(View rootview) {
        recycler_dates = (RecyclerView) rootview.findViewById(R.id.recycler_dates);
        date = (CompactCalendarView) rootview.findViewById(R.id.date);
        month = (TextView) rootview.findViewById(R.id.month);
        date.setUseThreeLetterAbbreviation(true);
        date.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                String setdate = new SimpleDateFormat("yyyy-MM-dd").format(dateClicked).toString();
                executescheduleList(setdate);
                //  AppDelegate.LogT("setdate=>>>>>" + setdate);*/
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                month.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });

        Calendar cal = Calendar.getInstance();
        month_date = new SimpleDateFormat("MMMM yyyy");
        String month_name = month_date.format(cal.getTime());
        month.setText("" + month_name);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //set_recycler_scheduled_List();
    }

    /*private void set_recycler_scheduled_List() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler_dates.setLayoutManager(layoutManager);
        Recycler_Calendar_dates_Adapter adapter = new Recycler_Calendar_dates_Adapter(getActivity(), scheduled_List);
        recycler_dates.setAdapter(adapter);
    }*/
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.months:

                break;
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.SCHEDULED_LIST)) {
            parseScheduledList(result);
        } else if (apiName.equals(ServerRequestConstants.SCHEDULED_DATES)) {
            parseScheduledDate(result);
        }
    }

    private void parseScheduledDate(String result) {
        try {
            JSONObject object = new JSONObject(result);
            ArrayList<String> dates = new ArrayList<>();
            if (object.getInt(Tags.status) == 1 && object.getInt(Tags.dataFlow) == 1) {
                JSONArray array = object.getJSONArray(Tags.response);
                for (int i = 0; i < array.length(); i++) {
                    AppDelegate.LogT("one ");
                    JSONObject obj = array.getJSONObject(i);
                    dates.add(obj.getString(Tags.training_date).replaceAll("/+0000", ""));
                }
                setdatesOnCalender(dates);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void addEvents(Date dateObject) {
//        currentCalender.set(Calendar.DAY_OF_MONTH, 1);
//        currentCalender.setTime(dateObject);
//
//        setToMidnight(currentCalender);
//
        AppDelegate.LogT("addEvents => " + dateObject);
        long timeInMillis = dateObject.getTime();
        AppDelegate.LogT("addEvents => 1 => " + dateObject.getTime());

        List<Event> events = getEvents(timeInMillis);


        date.addEvents(events);
        AppDelegate.LogT("getEvents => " + date.getEvents(timeInMillis).toString());
    }

    private List<Event> getEvents(long timeInMillis) {
        AppDelegate.LogT("getEvents => " + timeInMillis);
        return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)));
    }

    private List<Event> getEvents(long timeInMillis, int day) {
        if (day < 2) {
            return Arrays.asList(new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)));
        } else if (day > 2 && day <= 4) {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)));
        } else {
            return Arrays.asList(
                    new Event(Color.argb(255, 169, 68, 65), timeInMillis, "Event at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 100, 68, 65), timeInMillis, "Event 2 at " + new Date(timeInMillis)),
                    new Event(Color.argb(255, 70, 68, 65), timeInMillis, "Event 3 at " + new Date(timeInMillis)));
        }
    }

    private void setToMidnight(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private void setdatesOnCalender(ArrayList<String> dates) {
        for (int i = 0; i < dates.size(); i++) {
            try {
                Date simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(dates.get(i));
                AppDelegate.LogT("timeformate==>" + simpleDateFormat + "date");
                String formattedDate = new SimpleDateFormat("dd-MM-yyyy").format(simpleDateFormat);
                Date date = new SimpleDateFormat("dd-MM-yyyy").parse(formattedDate);
                addEvents(date);
            } catch (ParseException e) {
                AppDelegate.LogE(e);
            }
        }
        date.invalidate();
        logEventsByMonth(date);
    }

    private void executescheduleList(String setdate) {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.date, setdate + "");
                PostAsync mPostAsyncObj;
                mPostAsyncObj = new PostAsync(getActivity(), CalendarFragment.this, ServerRequestConstants.SCHEDULED_LIST,
                        mPostArrayList, null);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), "Please try again.", "Alert!!!");
        }
    }

    private void parseScheduledList(String result) {
        try {
            JSONObject object = new JSONObject(result);
            ArrayList<ScheduledListModel> scheduled_List = new ArrayList<>();
            if (object.getInt(Tags.status) == 1 && object.getInt(Tags.dataFlow) == 1) {
                JSONArray array = object.getJSONArray(Tags.response);
                for (int i = 0; i < array.length(); i++) {
                    ScheduledListModel scheduledmodel = new ScheduledListModel();
                    JSONObject obj = array.getJSONObject(i);
                    scheduledmodel.id = obj.getInt(Tags.id);
                    scheduledmodel.team_id = obj.getInt(Tags.team_id);
                    scheduledmodel.coach_id = obj.getInt(Tags.coach_id);
                    scheduledmodel.training_period = obj.getInt(Tags.training_period);
                    scheduledmodel.is_confirm = obj.getInt(Tags.is_confirm);
                    scheduledmodel.status = obj.getInt(Tags.status);
                    scheduledmodel.latitude = obj.getDouble(Tags.latitude);
                    scheduledmodel.longitude = obj.getDouble(Tags.longitude);
                    scheduledmodel.coach = obj.getString(Tags.coach);
                    scheduledmodel.teamName = obj.getString(Tags.teamName);
                    scheduledmodel.teamAvtar = obj.getString(Tags.teamAvtar);
                    scheduledmodel.training_date = obj.getString(Tags.training_date);
                    ArrayList<String> teammembers = new ArrayList<>();
                    JSONArray members = obj.getJSONArray(Tags.teamMembers);
                    for (int j = 0; j < members.length(); j++) {
                        String team = members.getString(j);
                        teammembers.add(team);
                    }
                    ArrayList<String> teammembersAvtar = new ArrayList<>();
                    JSONArray membersAvtar = obj.getJSONArray(Tags.teamMembersAvtar);
                    for (int j = 0; j < membersAvtar.length(); j++) {
                        String team = membersAvtar.getString(j);
                        teammembersAvtar.add(team);
                    }
                    scheduledmodel.teamMembers = teammembers;
                    scheduledmodel.teamMembersAvtar = teammembersAvtar;
                    scheduled_List.add(scheduledmodel);
                    /*if (object.has(Tags.avtar_thumb)) {
                        userDataModel.avtar_thumb = object.getString(Tags.avtar_thumb);
                    }*/
                }
                setdatesOnList(scheduled_List);
            } else if (object.getInt(Tags.status) == 0 && object.getInt(Tags.dataFlow) == 0) {

            }
        } catch (JSONException e) {
            // setdatesOnCalender(dates);
            e.printStackTrace();
        }
    }
    private void setdatesOnList(ArrayList<ScheduledListModel> scheduled_List) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler_dates.setLayoutManager(layoutManager);
        Recycler_Calendar_dates_Adapter adapter = new Recycler_Calendar_dates_Adapter(getActivity(), scheduled_List);
        recycler_dates.setAdapter(adapter);
    }
}
