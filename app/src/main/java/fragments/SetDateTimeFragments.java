package fragments;

import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import adapters.TeamListAdapter;
import butterknife.ButterKnife;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.PostAysnc_Model;
import model.TeamListModel;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class SetDateTimeFragments extends Fragment implements OnClickListener, AdapterView.OnItemClickListener, OnReciveServerResponse, OnDialogClickListener {
    private View rootview;
    RelativeLayout toolbar;
    TextView toolbar_title;
    ImageView tool_menu;
    RecyclerView team_list;
    private Bundle bundle;
    private TimePickerDialog timePicker;
    private int selectfromhour, selectfromminute;
    private String timeset;
    private TextView time;
    private CompactCalendarView date;
    private String setdate;
    private int team_id_type;
    private TextView month, txt_hour, txt_monthly;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMM - yyyy", Locale.getDefault());
    private RelativeLayout rel_tab_bar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.set_datetime, container, false);
        ButterKnife.bind(getActivity());
        bundle = getArguments();
        team_id_type = bundle.getInt(Tags.Find_Coach);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        toolbar = (RelativeLayout) rootview.findViewById(R.id.toolbar);
        rel_tab_bar = (RelativeLayout) rootview.findViewById(R.id.rel_tab_bar);
        toolbar_title = (TextView) rootview.findViewById(R.id.title);
        tool_menu = (ImageView) rootview.findViewById(R.id.back);
        tool_menu.setOnClickListener(this);
        time = (TextView) rootview.findViewById(R.id.time);
        txt_hour = (TextView) rootview.findViewById(R.id.hour);
        txt_monthly = (TextView) rootview.findViewById(R.id.monthly);
        date = (CompactCalendarView) rootview.findViewById(R.id.date);
        month = (TextView) rootview.findViewById(R.id.month);
        date.setUseThreeLetterAbbreviation(true);

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat month_date = new SimpleDateFormat("MMMM yyyy");
        String month_name = month_date.format(cal.getTime());
        month.setText("" + month_name);

        date.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {

                setdate = new SimpleDateFormat("dd/MM/yyyy").format(dateClicked).toString();
                set_time();
                AppDelegate.LogT("setdate=>>>>>" + setdate);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth) {
                month.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });
        time.setOnClickListener(this);
        rootview.findViewById(R.id.done).setOnClickListener(this);
        rel_tab_bar.setVisibility(View.GONE);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                getFragmentManager().popBackStack();
                break;
            case R.id.time:
                AppDelegate.LogT("time clicked");
                set_time();
                break;
            case R.id.done:
                if (AppDelegate.isValidString(timeset) && AppDelegate.isValidString(setdate)) {
                    execute_find_coach();
                } else if (!AppDelegate.isValidString(setdate)) {
                    AppDelegate.ShowDialog(getActivity(), "Please select date.", "Time out!!!");
                } else {
                    if (!AppDelegate.isValidString(setdate)) {
                        AppDelegate.ShowDialog(getActivity(), "Please select time.", "Time out!!!");
                    }
                }
                break;
        }
    }

    private void execute_find_coach() {

        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.latitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LAT, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Tags.longitude, new Prefs(getActivity()).getStringValue(Tags.TAG_LONG, ""));
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.team_id, team_id_type, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.training_date, setdate + " " + timeset);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.deviceType, 2, ServerRequestConstants.Key_PostintValue);
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.training_period, 1, ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(getActivity(), SetDateTimeFragments.this, ServerRequestConstants.FIND_COACH,
                        mPostArrayList, SetDateTimeFragments.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.try_again), "Alert!!!");
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {

        }
    }

    private void set_time() {
        timePicker = new TimePickerDialog(getContext(), R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                selectfromhour = selectedHour;
                selectfromminute = selectedMinute;
                if (selectedHour < 12 && selectedHour >= 0) {
                    time.setText(selectedHour + ":" + selectedMinute + "AM");
                    timeset = selectedHour + ":" + selectedMinute + "AM";
                } else {
                    selectedHour -= 12;
                    if (selectedHour == 0) {
                        selectedHour = 12;
                    }
                    time.setText(selectedHour + ":" + selectedMinute + "PM");
                    timeset = selectedHour + ":" + selectedMinute + "PM";
                }

            }
        }, selectfromhour, selectfromminute, false);//Yes 24 hour time
        timePicker.show();

        AppDelegate.LogT("date=>..." + timeset);
    }


    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.FIND_COACH)) {
            if (team_id_type == 0) {
                parseFitTeamList(result);
            } else {
                parseCreateTeamList(result);
            }
        } else if (apiName.equals(ServerRequestConstants.CREATE_TEAM)) {
            parseCreateTeamList(result);
        }
    }

    private void parseCreateTeamList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
//            AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, SetDateTimeFragments.this);
            //int request_id = jsonObject.getInt("request_id");
            /*
            if (jsonObject.getInt(Tags.status) == 1) {

            } else {
                if (jsonObject.getInt(Tags.status) == 0) {
                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, SetDateTimeFragments.this);
                } else if (jsonObject.has(Tags.response) && jsonObject.optJSONObject(Tags.response) != null *//*&& jsonObject.getJSONObject(Tags.response).has(Tags.suspand_status) && jsonObject.getJSONObject(Tags.response).optString(Tags.suspand_status) != null*//*) {
                    AppDelegate.ShowDialogID(getActivity(), jsonObject.getString(Tags.message), "Alert", Tags.ok, SetDateTimeFragments.this);
                }
            }*/
            getFragmentManager().popBackStack();
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }

    private void parseFitTeamList(String result) {
        try {
            JSONObject jsonObject = new JSONObject(result);
            AppDelegate.showToast(getActivity(), jsonObject.getString(Tags.message));
            int team_id = jsonObject.getInt(Parameters.team_id);
            getFragmentManager().popBackStack();
        } catch (Exception e) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.response_error), "");
            AppDelegate.LogE(e);
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {

    }


}
