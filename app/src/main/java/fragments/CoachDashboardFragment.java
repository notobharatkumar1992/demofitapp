package fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.fitplus.SurveyFormActivity;
import com.fitplus.UploadDocsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Async.PostAsync;
import Constants.Parameters;
import Constants.ServerRequestConstants;
import Constants.Tags;
import interfaces.OnDialogClickListener;
import interfaces.OnReciveServerResponse;
import model.BecomeCoachModel;
import model.PostAysnc_Model;
import utils.Prefs;

/**
 * Created by admin on 26-07-2016.
 */
public class CoachDashboardFragment extends Fragment implements OnClickListener, OnReciveServerResponse, OnDialogClickListener {
    private View rootview;
    TextView findCoach, Become_Coach;
    private BecomeCoachModel become_coach;
    Bundle bundle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.find, container, false);

        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        view.findViewById(R.id.find_coach).setOnClickListener(this);
        view.findViewById(R.id.be_coach).setOnClickListener(this);
        view.findViewById(R.id.radar).setOnClickListener(this);
        view.findViewById(R.id.menu).setOnClickListener(this);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.find_coach:
                bundle = new Bundle();
                bundle.putInt(Tags.Find_Coach, Tags.Prefindcoach);
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new SetDateTimeFragments(), R.id.container, bundle, null);
                break;
            case R.id.be_coach:
                execute_Become_Coach();
                break;
            case R.id.radar:
                AppDelegate.showFragmentAnimation(getActivity().getSupportFragmentManager(), new RadarFragment(), R.id.container);
                break;

            case R.id.menu:
                ((com.fitplus.MainActivity) getActivity()).toggleSlider();
                break;

        }
    }

    private void execute_Become_Coach() {
        try {
            if (AppDelegate.haveNetworkConnection(getActivity(), true)) {
                ArrayList<PostAysnc_Model> mPostArrayList = new ArrayList<PostAysnc_Model>();
                PostAsync mPostAsyncObj;
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.API_KEY, Parameters.API_KEY_VALUE.trim());
                AppDelegate.getInstance(getActivity()).setPostParamsSecond(mPostArrayList, Parameters.user_id, new Prefs(getActivity()).getUserdata().userId);
                //  AppDelegate.getInstance(this).setPostParamsSecond(mPostArrayList, Parameters.device_token, AppDelegate.getValue(SignupActivity.this, Tags.REGISTRATION_ID), ServerRequestConstants.Key_PostintValue);
                mPostAsyncObj = new PostAsync(getActivity(), CoachDashboardFragment.this, ServerRequestConstants.BECOME_COACH,
                        mPostArrayList, CoachDashboardFragment.this);
                AppDelegate.showProgressDialog(getActivity());
                mPostAsyncObj.execute();
            }
        } catch (Exception e) {
            AppDelegate.LogE(e);
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.try_again), "Alert!!!");
        }
    }

    @Override
    public void setOnReciveResult(String apiName, String result) {
        AppDelegate.hideProgressDialog(getActivity());
        if (!AppDelegate.isValidString(result)) {
            AppDelegate.ShowDialog(getActivity(), getActivity().getResources().getString(R.string.time_out), "Time out!!!");
            return;
        }
        if (apiName.equals(ServerRequestConstants.BECOME_COACH)) {
            parseBecome_coach(result);
        }
    }

    private void parseBecome_coach(String result) {
        try {
            JSONObject response = new JSONObject(result);
            response.getString("message");
            ArrayList<BecomeCoachModel> becomeCoachModels = new ArrayList<>();
            AppDelegate.showToast(getActivity(), response.getString("message"));
            become_coach = new BecomeCoachModel();
            become_coach.status = response.getInt(Tags.status);
            become_coach.dataflow=response.getInt(Tags.dataFlow);
            become_coach.doc_status = response.getInt("doc_status");
            if (become_coach.status == 0 &&become_coach.dataflow == 1 && become_coach.doc_status == 0) {
                AppDelegate.LogT("condition 1");
                Intent intent = new Intent(getActivity(), UploadDocsActivity.class);
                startActivity(intent);
            } else if (become_coach.status == 0 && become_coach.dataflow == 1 && become_coach.doc_status == 1) {
                AppDelegate.LogT("condition 2");
            } else if (become_coach.status == 0 && become_coach.dataflow == 1 && become_coach.doc_status == 2) {
                try {
                    JSONArray jsonArray = response.getJSONArray("response");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        become_coach = new BecomeCoachModel();
                        become_coach.id = obj.getInt("id");
                        become_coach.question = obj.getString("question");
                        become_coach.option1 = obj.getString("option1");
                        become_coach.option2 = obj.getString("option2");
                        become_coach.option3 = obj.getString("option3");
                        become_coach.option4 = obj.getString("option4");
                        become_coach.answer = obj.getString("answer");
                        become_coach.question_status = obj.getInt("status");
                        become_coach.answer_status = 0;
                        becomeCoachModels.add(become_coach);
                    }
                    AppDelegate.showToast(getActivity(), response.getString("message"));
                    bundle = new Bundle();
                    bundle.putParcelableArrayList(Tags.parcel_becomeCoach, becomeCoachModels);
                } catch (Exception e) {
                    AppDelegate.LogE(e);
                }
                AppDelegate.LogT("condition 3");
                Intent intent = new Intent(getActivity(), SurveyFormActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            } else if (become_coach.status == 1  && become_coach.dataflow == 2 && become_coach.doc_status == 1) {
                AppDelegate.LogT("condition 4");
            } else {
                AppDelegate.LogT("condition 5");
            }


          /*  if (become_coach.status == 0 && become_coach.doc_status == 0) {
                Intent intent = new Intent(getActivity(), SurveyFormActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            } else if (become_coach.status == 1 && become_coach.doc_status == 0) {

            } else if (become_coach.status == 2 && become_coach.doc_status == 0) {
                Intent intent = new Intent(getActivity(), UploadDocsActivity.class);
                startActivity(intent);
            } else if (become_coach.status == 2 && become_coach.doc_status == 1) {
            } else {
            }*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setOnDialogClickListener(String name) {
//        if (name.equalsIgnoreCase(Tags.ok)) {
//            if (become_coach.status == 0 && become_coach.doc_status == 0) {
//                Intent intent = new Intent(getActivity(), SurveyFormActivity.class);
//                intent.putExtras(bundle);
//                startActivity(intent);
//            } else if (become_coach.status == 1 && become_coach.doc_status == 0) {
//
//            } else if (become_coach.status == 2 && become_coach.doc_status == 0) {
//                Intent intent = new Intent(getActivity(), UploadDocsActivity.class);
//                startActivity(intent);
//            } else if (become_coach.status == 2 && become_coach.doc_status == 1) {
//
//            } else {
//
//            }
//        }
    }
}
