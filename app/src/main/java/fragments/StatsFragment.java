package fragments;



import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.fitplus.R;

import adapters.MonthsAdapter;
import adapters.Recycler_Stats_Adapter;

/**
 * Created by admin on 26-07-2016.
 */
public class StatsFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private RecyclerView months,recycler_stats;
    String[] monthArray;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.stats, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View rootview) {
        months = (RecyclerView) rootview.findViewById(R.id.months);
        recycler_stats = (RecyclerView) rootview.findViewById(R.id.recycler_stats);
        monthArray = new String[]{getResources().getString(R.string.jan), getResources().getString(R.string.feb), getResources().getString(R.string.mar), getResources().getString(R.string.apr)
                , getResources().getString(R.string.may), getResources().getString(R.string.jun), getResources().getString(R.string.jul), getResources().getString(R.string.aug), getResources().getString(R.string.sep), getResources().getString(R.string.oct), getResources().getString(R.string.nov), getResources().getString(R.string.dec)};
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setmonth();
        set_recycler_stats();
    }

    private void set_recycler_stats() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recycler_stats.setLayoutManager(layoutManager);
        Recycler_Stats_Adapter adapter = new Recycler_Stats_Adapter(getActivity());
        recycler_stats.setAdapter(adapter);
    }

    private void setmonth() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        months.setLayoutManager(layoutManager);
        MonthsAdapter adapter = new MonthsAdapter(getActivity(), monthArray);
        months.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (view.getId()) {
            case R.id.months:

                break;
        }
    }
}
