package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 09-08-2016.
 */
public class BecomeCoachModel implements Parcelable {
   public  int status,dataflow,doc_status,id,question_status,answer_status,answer_number;
   public String question,option1,option2,option3,option4,v,modified,answer,message,given_answer;

    public BecomeCoachModel() {
    }

    public BecomeCoachModel(Parcel in) {
        status = in.readInt();
        dataflow = in.readInt();
        doc_status = in.readInt();
        id = in.readInt();
        question_status = in.readInt();
        question = in.readString();
        option1 = in.readString();
        option2 = in.readString();
        option3 = in.readString();
        option4 = in.readString();
        v = in.readString();
        modified = in.readString();
        answer = in.readString();
        message=in.readString();
        answer_status=in.readInt();
        given_answer=in.readString();
        answer_number=in.readInt();
    }

    public static final Creator<BecomeCoachModel> CREATOR = new Creator<BecomeCoachModel>() {
        @Override
        public BecomeCoachModel createFromParcel(Parcel in) {
            return new BecomeCoachModel(in);
        }

        @Override
        public BecomeCoachModel[] newArray(int size) {
            return new BecomeCoachModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataflow);
        dest.writeInt(doc_status);
        dest.writeInt(id);
        dest.writeInt(question_status);
        dest.writeString(question);
        dest.writeString(option1);
        dest.writeString(option2);
        dest.writeString(option3);
        dest.writeString(option4);
        dest.writeString(v);
        dest.writeString(modified);
        dest.writeString(answer);
        dest.writeString(message);
        dest.writeInt(answer_status);
        dest.writeString(given_answer);
        dest.writeInt(answer_number);
    }
}
