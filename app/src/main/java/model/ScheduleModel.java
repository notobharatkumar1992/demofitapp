package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 31-08-2016.
 */
public class ScheduleModel implements Parcelable {
    public int status, dataFlow,user1_action,user1_id,user2_id,user3_id,user2_action,user3_action,id;
    public String user1_date,training_date,request_type,team_id,message;

    public ScheduleModel() {
    }

    @Override
    public String toString() {
        return "ScheduleModel{" +
                "status=" + status +
                ", dataFlow=" + dataFlow +
                ", user1_action=" + user1_action +
                ", user1_id=" + user1_id +
                ", user2_id=" + user2_id +
                ", user3_id=" + user3_id +
                ", user2_action=" + user2_action +
                ", user3_action=" + user3_action +
                ", id=" + id +
                ", user1_date='" + user1_date + '\'' +
                ", training_date='" + training_date + '\'' +
                ", request_type='" + request_type + '\'' +
                ", team_id='" + team_id + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    protected ScheduleModel(Parcel in) {
        status = in.readInt();
        dataFlow = in.readInt();
        user1_action = in.readInt();
        user1_id = in.readInt();
        user2_id = in.readInt();
        user3_id = in.readInt();
        user2_action = in.readInt();
        user3_action = in.readInt();
        id = in.readInt();
        user1_date = in.readString();
        training_date = in.readString();
        request_type = in.readString();
        team_id = in.readString();
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(dataFlow);
        dest.writeInt(user1_action);
        dest.writeInt(user1_id);
        dest.writeInt(user2_id);
        dest.writeInt(user3_id);
        dest.writeInt(user2_action);
        dest.writeInt(user3_action);
        dest.writeInt(id);
        dest.writeString(user1_date);
        dest.writeString(training_date);
        dest.writeString(request_type);
        dest.writeString(team_id);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ScheduleModel> CREATOR = new Creator<ScheduleModel>() {
        @Override
        public ScheduleModel createFromParcel(Parcel in) {
            return new ScheduleModel(in);
        }

        @Override
        public ScheduleModel[] newArray(int size) {
            return new ScheduleModel[size];
        }
    };
}
