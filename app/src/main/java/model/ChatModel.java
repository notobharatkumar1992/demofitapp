package model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by admin on 22-08-2016.
 */
public class ChatModel implements Parcelable {

    public static final int FROM_USER = 1, FROM_OTHER = 2, FROM_ACTION = 3;
    public int status, chat_status, dataFlow, team_id, id;
    public String txt_msg_send, txt_time_send, txt_msg_rcv, txt_time_rcv, txt_name_rcv, txt_pic_rcv, team_name, team_pic, action_message, action_time, action_user_id, action_by;

    public ChatModel() {
    }

    public ChatModel(Parcel in) {
        status = in.readInt();
        chat_status = in.readInt();
        dataFlow = in.readInt();
        team_id = in.readInt();
        id = in.readInt();
        txt_msg_send = in.readString();
        txt_time_send = in.readString();
        txt_msg_rcv = in.readString();
        txt_time_rcv = in.readString();
        txt_name_rcv = in.readString();
        txt_pic_rcv = in.readString();
        team_name = in.readString();
        team_pic = in.readString();
    }

    public ChatModel(int team_id) {
        this.team_id = team_id;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeInt(chat_status);
        dest.writeInt(dataFlow);
        dest.writeInt(team_id);
        dest.writeInt(id);
        dest.writeString(txt_msg_send);
        dest.writeString(txt_time_send);
        dest.writeString(txt_msg_rcv);
        dest.writeString(txt_time_rcv);
        dest.writeString(txt_name_rcv);
        dest.writeString(txt_pic_rcv);
        dest.writeString(team_name);
        dest.writeString(team_pic);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ChatModel> CREATOR = new Creator<ChatModel>() {
        @Override
        public ChatModel createFromParcel(Parcel in) {
            return new ChatModel(in);
        }

        @Override
        public ChatModel[] newArray(int size) {
            return new ChatModel[size];
        }
    };
}
