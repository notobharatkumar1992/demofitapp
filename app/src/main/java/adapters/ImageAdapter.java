package adapters;

import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.fitplus.AppDelegate;


import java.util.ArrayList;
import java.util.Random;

import Constants.Tags;
import interfaces.OnListItemClickListener;

public class ImageAdapter extends PagerAdapter {

    private final Random random = new Random();
    private ArrayList sliderArray;
    private OnListItemClickListener onListItemClickListener;

    public ImageAdapter() {
    }

    public ImageAdapter(ArrayList sliderArray, OnListItemClickListener onListItemClickListener) {
        this.sliderArray = sliderArray;
        this.onListItemClickListener = onListItemClickListener;
    }

    @Override
    public int getCount() {
        return sliderArray.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        final ImageView imageView = new ImageView(view.getContext());
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onListItemClickListener != null)
                    onListItemClickListener.setOnListItemClickListener(Tags.LIST_ITEM_TRENDING, position);
            }
        });
       /* AppDelegate.getInstance(view.getContext()).getImageLoader().get(String.valueOf(sliderArray.get(position)), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AppDelegate.LogE(error);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null)
                    imageView.setImageBitmap(response.getBitmap());

            }
        });*/
        imageView.setImageResource((Integer) sliderArray.get(position));
        //Picasso.with(view.getContext()).load(sliderArray.get(position).getImage1()).into(imageView);

        view.addView(imageView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        return imageView;
    }

//    public void addItem() {
//        mSize++;
//        notifyDataSetChanged();
//    }
//
//    public void removeItem() {
//        mSize--;
//        mSize = mSize < 0 ? 0 : mSize;
//
//        notifyDataSetChanged();
//    }
}