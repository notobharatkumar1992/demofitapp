package adapters;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fitplus.AppDelegate;
import com.fitplus.R;
import com.fitplus.SetLocationActivity;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Async.LocationAddress;
import Constants.Tags;
import model.ChatModel;
import model.ScheduledListModel;


public class Recycler_Calendar_dates_Adapter extends RecyclerView.Adapter<Recycler_Calendar_dates_Adapter.ViewHolder> {
    View v;
    FragmentActivity context;
    ArrayList<ScheduledListModel> scheduled_List;
    private Handler mHandler;
    private String address;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.calendar_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        setHandler();
        return viewHolder;
    }

    private void setloc(double latitude, double longitude) {
        LocationAddress.getAddressFromLocation(latitude, longitude, context, mHandler);
    }

    private void setHandler() {
        mHandler = new Handler() {
            @Override
            public void dispatchMessage(Message msg) {
                super.dispatchMessage(msg);
                switch (msg.what) {
                    case 0:
                        break;
                    case 2:
                        setAddressFromGEOcoder(msg.getData());
                        break;
                }
            }
        };
    }

    private void setAddressFromGEOcoder(Bundle data) {
        address = data.getString(Tags.ADDRESS) + "";
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        AppDelegate.LogT("ViewHolder" + position + "");
        final ScheduledListModel scheduledListModel = scheduled_List.get(position);

        try {
            Date simpleDateFormat = new SimpleDateFormat("HH:mm:ss").parse(scheduledListModel.training_date);
            String formattedDate = new SimpleDateFormat("HH:mm").format(simpleDateFormat);
            holder.txt_starttime.setText(formattedDate);
            holder.txt_workout_time.setText(formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (!scheduledListModel.teamMembers.isEmpty()) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < scheduledListModel.teamMembers.size(); i++) {
                sb.append(scheduledListModel.teamMembers.get(i)).append(" ,");
            }
            String users = sb.toString();
            users = users.substring(0, users.length() - 1);
            holder.txt_username.setText(users);
        }
        holder.extra.setText(String.valueOf(scheduledListModel.teamMembers.size()) + "");
        setloc(scheduledListModel.latitude, scheduledListModel.longitude);
        holder.location.setText(address);
        holder.txt_work_out.setText(scheduledListModel.teamName + "");
        holder.img_follower_one.setVisibility(View.GONE);
        holder.img_follower_two.setVisibility(View.GONE);
        holder.img_follower_three.setVisibility(View.GONE);
        holder.img_follower_four.setVisibility(View.GONE);
        if(!scheduledListModel.teamMembersAvtar.isEmpty()) {
            for (int i = 0; i < scheduledListModel.teamMembersAvtar.size(); i++) {
                final int finalI = i;
                Picasso.with(context).load(scheduledListModel.teamMembersAvtar.get(i)).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        AppDelegate.LogT("onBitmapLoaded" + scheduledListModel.teamMembersAvtar.get(finalI));
                        if (finalI == 0) {
                            holder.img_follower_one.setVisibility(View.VISIBLE);
                            holder.img_follower_one.setImageBitmap(bitmap);
                        } else if (finalI == 1) {
                            holder.img_follower_two.setVisibility(View.VISIBLE);
                            holder.img_follower_two.setImageBitmap(bitmap);
                        } else if (finalI == 2) {
                            holder.img_follower_three.setVisibility(View.VISIBLE);
                            holder.img_follower_three.setImageBitmap(bitmap);
                        } else if (finalI == 3) {
                            holder.img_follower_four.setVisibility(View.VISIBLE);
                            holder.img_follower_four.setImageBitmap(bitmap);
                        }
                    }
                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        AppDelegate.LogT("onBitmapFailed" + scheduledListModel.teamMembersAvtar.size());
                    }
                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                        AppDelegate.LogT("onPrepareLoad" + scheduledListModel.teamMembersAvtar.size());
                    }
                });
            }
        }
    }

    public Recycler_Calendar_dates_Adapter(FragmentActivity context, ArrayList<ScheduledListModel> scheduled_List) {
        this.context = context;
        this.scheduled_List = scheduled_List;
        AppDelegate.LogT("scheduled_List  " + scheduled_List.size() + "");
    }

    @Override
    public int getItemCount() {
        return scheduled_List.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_starttime, txt_workout_time, txt_work_out, txt_username, extra, location;
        ImageView img_follower_one, img_follower_two, img_follower_three, img_follower_four;

        public ViewHolder(View itemView) {
            super(itemView);
            txt_starttime = (TextView) itemView.findViewById(R.id.txt_starttime);
            txt_workout_time = (TextView) itemView.findViewById(R.id.txt_workout_time);
            txt_work_out = (TextView) itemView.findViewById(R.id.txt_work_out);
            txt_username = (TextView) itemView.findViewById(R.id.txt_username);
            location = (TextView) itemView.findViewById(R.id.location);
            extra = (TextView) itemView.findViewById(R.id.extra);
            img_follower_one = (ImageView) itemView.findViewById(R.id.img_follower_one);
            img_follower_two = (ImageView) itemView.findViewById(R.id.img_follower_two);
            img_follower_three = (ImageView) itemView.findViewById(R.id.img_follower_three);
            img_follower_four = (ImageView) itemView.findViewById(R.id.img_follower_four);
        }
    }
}