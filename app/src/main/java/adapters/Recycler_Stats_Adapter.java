package adapters;


import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fitplus.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Recycler_Stats_Adapter extends RecyclerView.Adapter<Recycler_Stats_Adapter.ViewHolder> {
    View v;
    FragmentActivity context;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.stats_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // bundle=new Bundle();
        //  final PointsHistoryModel dv = pointsHistory.get(position);

    }
    public Recycler_Stats_Adapter(FragmentActivity context) {
        this.context = context;

    }
    @Override
    public int getItemCount() {
        return 10;
    }
    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(View itemView) {
            super(itemView);

        }
    }
}